import { getClient, imageBuilder } from "@lib/sanity";
import { groq } from "next-sanity";
import Layout from "@components/Layout";
import NarrowContainer from "@components/Layout/NarrowContainer";
import Prose from "@components/Layout/Prose";
import { H1, H2 } from "@components/Layout/Text";

const siteConfigQuery = groq`*[_type == "siteconfig"][0]`;

type PostProps = {
  slug: Slug;
  siteConfigData: any;
  postData: Post;
};

export default function Post(props: PostProps) {
  const { siteConfigData, postData } = props;
  return (
    postData && (
      <Layout
        headerTitle={`${postData.title} | ${siteConfigData.title}`}
        title={postData.title}
        description={postData.description}
        image={postData.image}
        siteConfig={siteConfigData}
      >
        <div className="py-24">
          <NarrowContainer>
            <H1>{postData.title}</H1>
            <H2>by {postData.author.map((author) => author.name).join(", ")}</H2>
            <Prose body={postData.body} />
          </NarrowContainer>
        </div>
      </Layout>
    )
  );
}

export async function getStaticPaths() {
  const slugs = await getClient().fetch(groq`*[_type == 'post']{slug}`);
  return {
    paths: slugs.map((obj: { slug: { current: string } }) => ({
      params: {
        slug: obj.slug.current,
      },
    })),
    fallback: false,
  };
}

export async function getStaticProps({ params }) {
  const postQuery = groq`*[_type == "post" && slug.current == "${params.slug}"]{
    ...,
    body[]{
      ...,
      markDefs[]{
        ...,
        _type == "internalLink" => {
          _key,
          "slug": @.reference->slug,
          "type": @.reference->_type,
        }
      }
    },
    author[]->
  }[0]`;
  const siteConfig = await getClient().fetch(siteConfigQuery);
  const post = await getClient().fetch(postQuery);
  return {
    props: {
      slug: params.slug,
      siteConfigData: siteConfig,
      postData: post,
    },
    revalidate: 10,
  };
}

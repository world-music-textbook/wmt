import { NextApiRequest } from "next";
import { getClient } from "../../lib/sanity";

export const articleBySlugQuery = `
  *[_type == "article" && slug.current == $slug][0] {
    "slug": slug.current
  }
`;

function redirectToPreview(res, Location) {
  // Enable preview mode by setting the cookies
  res.setPreviewData({});

  // Redirect to a preview capable route
  res.writeHead(307, { Location });
  res.end();
}

export default async function preview(req: NextApiRequest, res) {
  const { secret, slug } = req.query;

  if (!secret) {
    return res.status(401).json({ message: "No secret token" });
  }

  if (secret !== process.env.SANITY_PREVIEW_SECRET) {
    return res.status(401).json({ message: "Invalid secret" });
  }

  if (!slug) {
    return redirectToPreview(res, "/");
  }

  /*
   This section needs updating with better logic to handle incoming slugs/routing determinations
  */

  // // Check if the document with the given `slug` exists
  // const article = await getClient(true).fetch(articleBySlugQuery, { slug });

  // // If the slug doesn't exist prevent preview mode from being enabled
  // if (!article) {
  //   return res.status(401).json({ message: "Invalid slug" });
  // }

  // Redirect to the path from the fetched article
  // We don't redirect to req.query.slug as that might lead to open redirect vulnerabilities
  // return redirectToPreview(res, `/${article.slug}`);
  return redirectToPreview(res, `/`);
}

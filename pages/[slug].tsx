import { getClient } from "@lib/sanity";
import { groq } from "next-sanity";
import Layout from "@components/Layout";
import NarrowContainer from "@components/Layout/NarrowContainer";
import Prose from "@components/Layout/Prose";

const siteConfigQuery = groq`*[_type == "siteconfig"][0]`;

type PageProps = {
  slug: { current: string };
  siteConfigData: any;
  pageData: any;
};

export default function Page(props: PageProps) {
  const { slug, siteConfigData, pageData } = props;
  return (
    <Layout
      headerTitle={`${[pageData.title, pageData.titlePart2].join(" ")} | ${
        siteConfigData.title
      }`}
      title={pageData.title}
      titlePart2={pageData.titlePart2}
      description={pageData.description}
      image={pageData.image}
      cta={pageData.cta}
      siteConfig={siteConfigData}
    >
      <div className="py-24">
        <NarrowContainer>
          <Prose body={pageData.body} />
        </NarrowContainer>
      </div>
    </Layout>
  );
}

export async function getStaticPaths() {
  return {
    paths: [{ params: { slug: "about" } }, { params: { slug: "call" } }],
    fallback: false,
  };
}

export async function getStaticProps({ params }) {
  const pageQuery = groq`*[_type == "page" && slug.current == "${params.slug}"]{
    ...,
    body[]{
      ...,
      markDefs[]{
        ...,
        _type == "internalLink" => {
          _key,
          "slug": @.reference->slug,
          "type": @.reference->_type,
        }
      }
    }
  }[0]`;
  const siteConfig = await getClient().fetch(siteConfigQuery);
  const page = await getClient().fetch(pageQuery);
  return {
    props: {
      slug: params.slug,
      siteConfigData: siteConfig,
      pageData: page,
    },
    revalidate: 10,
  };
}

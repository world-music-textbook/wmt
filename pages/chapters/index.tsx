import { getClient } from "../../lib/sanity";
import { groq } from "next-sanity";
import Layout from "@components/Layout";
import NarrowContainer from "@components/Layout/NarrowContainer";
import Prose from "@components/Layout/Prose";
import Button from "@components/UI/Button";
import Filter from "@components/UI/Filter";

const chapterIndexQuery = groq`{
  "siteConfig": *[_type == "siteconfig"][0],
  "pageData": *[_type == "page" && slug.current == "chapters"][0],
  "chapterData": *[_type == "chapter"] | order(publishedAt desc) {
    ..., 
    author[]->,
    tags[]->
  }
}`;

type ChapterPageProps = {
  chapterIndexData: {
    siteConfig: any;
    pageData: any;
    chapterData: any[];
  };
};

export default function Home(props: ChapterPageProps) {
  const { chapterIndexData } = props;
  const { siteConfig, pageData, chapterData } = chapterIndexData;
  return (
    <Layout
      headerTitle={`${[pageData.title, pageData.titlePart2].join(" ")} | ${
        siteConfig.title
      }`}
      title={pageData.title}
      titlePart2={pageData.titlePart2}
      description={pageData.description}
      image={pageData.image}
      siteConfig={siteConfig}
    >
      <NarrowContainer>
        <div className="py-24">
          <Prose body={pageData.body} />
        </div>
        <div className="pb-24 flex justify-center space-x-12">
          <Button link="/call" text="Contribute a chapter" />
          <Button link="/resources" text="Other resources" />
          <Button link="/about" text="Learn more" />
        </div>
      </NarrowContainer>
      <Filter chapters={chapterData} />
      <NarrowContainer>
        <div className="py-24 flex justify-center space-x-12">
          <Button link="/call" text="Contribute a chapter" />
          <Button link="/resources" text="Other resources" />
          <Button link="/about" text="Learn more" />
        </div>
      </NarrowContainer>
    </Layout>
  );
}

export async function getStaticProps() {
  const chapterPage = await getClient().fetch(chapterIndexQuery);
  return {
    props: {
      chapterIndexData: chapterPage,
    },
    revalidate: 10,
  };
}

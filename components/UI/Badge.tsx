export default function Badge(props: { text: string; color?: string }) {
  const { text, color } = props;
  let bg: string;
  let txt: string;
  switch (color) {
    case "red":
      bg = "bg-red-100";
      txt = "text-red-800";
      break;
    case "blue":
      bg = "bg-blue-100";
      txt = "text-blue-800";
      break;
    case "green":
      bg = "bg-green-100";
      txt = "text-green-800";
      break;
    case "yellow":
      bg = "bg-yellow-100";
      txt = "text-yellow-800";
      break;
    default:
      bg = "bg-blue-100";
      txt = "text-blue-800";
      break;
  }
  return (
    <span
      className={`inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium ${bg} ${txt}`}
    >
      {text}
    </span>
  );
}

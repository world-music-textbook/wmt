import Image from "next/image";
import Prose from "@components/Layout/Prose";
import { imageBuilder } from "../../lib/sanity";

type AuthorProps = {
  author: Author;
};

export default function Author(props: AuthorProps) {
  const { author } = props;
  return author.image ? (
    <div className="flex">
      <div className="flex-1 flex items-center justify-between">
        <div className="flex-1 border-l-2 border-slate-900 rounded-l-md px-3 pt-1 pb-2 bg-slate-50">
          <div className="">
            <h3 className="font-bold text-xl text-slate-900">{author.name}</h3>
            {author.role && (
              <span className="font-light text-lg text-slate-600">
                {author.role}
              </span>
            )}
            {author.role && author.affiliation ? (
              <span className="font-light text-lg text-slate-600"> | </span>
            ) : null}
            {author.affiliation && (
              <span className="font-light text-lg text-slate-600">
                {author.affiliation}
              </span>
            )}
            {author.bio && (
              <div className="italic text-sm">
                <Prose body={author.bio} />
              </div>
            )}
          </div>
        </div>
      </div>
      <div
        className={
          "relative flex-shrink-0 flex items-center justify-center w-28 min-h-28 text-white text-sm font-medium rounded-r-md"
        }
      >
        <Image
          layout="fill"
          objectFit="cover"
          src={imageBuilder(author.image).url()}
          className="rounded-r-md"
          alt="Author photo"
        />
      </div>
    </div>
  ) : (
    <div className="bg-slate-50 pb-2 pt-1 px-3 border-l-2 border-slate-900 rounded-md">
      <h3 className="font-bold text-xl text-slate-900">{author.name}</h3>
      {author.role && (
        <span className="font-light text-lg text-slate-600">{author.role}</span>
      )}
      {author.role && author.affiliation ? (
        <span className="font-light text-lg text-slate-600"> | </span>
      ) : null}
      {author.affiliation && (
        <span className="font-light text-lg text-slate-600">
          {author.affiliation}
        </span>
      )}
      {author.bio && (
        <div className="italic text-sm">
          <Prose body={author.bio} />
        </div>
      )}
    </div>
  );
}

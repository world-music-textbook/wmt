import Link from "next/link";
import { HiOutlineTag } from "react-icons/hi";
import Prose from "../Layout/Prose";
import { getTagList } from "../../lib/utils";

type ResourceListingProps = {
  resource: Resource;
};

export default function ResourceListing(props: ResourceListingProps) {
  const { resource } = props;
  const heading = (
    <>
      <p className="inline-block border-b-4 border-slate-500 font-light">
        {resource.date}
      </p>
      {/* format title: book gets italics, journal article gets quotes */}
      <h2
        className={`text-2xl font-bold ${
          resource.type === "Book" ? "italic" : null
        }`}
      >
        {resource.type === "Journal article"
          ? `"${resource.title}"`
          : resource.title}
      </h2>
      {resource.author || resource.publisher ? (
        <div className="font-light">
          {resource.author}
          {resource.author && resource.publisher ? " / " : null}
          {resource.publisher}
        </div>
      ) : null}
    </>
  );
  return (
    <article
      className={
        resource.link
          ? "transition duration-300 hover:bg-slate-100 hover:text-slate-900 rounded-md"
          : null
      }
    >
      <div className="px-4 pb-4 pt-3 text-slate-500">
        {resource.link ? (
          <Link href={resource.link}>
            <a target="_blank" className="cursor-pointer">
              {heading}
            </a>
          </Link>
        ) : (
          heading
        )}
        {resource.description && (
          <div className="mt-2 text-sm font-light italic">
            <Prose body={resource.description} />
          </div>
        )}
        {resource.tags && (
          <p className="mt-2 text-sm font-light">
            <HiOutlineTag
              className="inline align-middle text-gray-500 h-4 w-4 mr-1"
              aria-hidden="true"
            />
            {getTagList(resource.tags)}
          </p>
        )}
      </div>
    </article>
  );
}

import NarrowContainer from "@components/Layout/NarrowContainer";

type UnfinishedProps = { body: string };

export default function Unfinished(props: UnfinishedProps) {
  return (
    <NarrowContainer>
      <div className="bg-red-100 text-lg font-bold p-4 rounded-md">
        {props.body}
      </div>
    </NarrowContainer>
  );
}

import { useState, ReactElement } from "react";
import { toPlainText } from "@portabletext/react";
import { AiOutlineArrowLeft, AiOutlineArrowRight } from "react-icons/ai";
import NarrowContainer from "@components/Layout/NarrowContainer";
import ChapterListContainer from "@components/Layout/ChapterListContainer";
import ListContainer from "@components/Layout/ListContainer";
import ResourceListing from "@components/UI/ResourceListing";

const includeResource = (resource: Resource | null, filter: string) => {
  if (!resource) return null;
  const toSearch = [
    resource.title,
    resource.author,
    resource.publisher,
    resource.description?.map((d) => toPlainText(d).toLowerCase()).join(" "),
    resource.tags.map((t) => t.name).join(" "),
    resource.link,
    resource.type,
  ];
  // check resource tags against selected tags
  return toSearch.join(" ").toLowerCase().includes(filter.toLowerCase());
};

const includeChapter = (chapter: Chapter | null, filter: string) => {
  if (!chapter) return null;
  const toSearch = [
    chapter.title,
    chapter.author.map((author) => author.name),
    chapter.description,
    chapter.tags.map((t) => t.name).join(" "),
  ];
  return toSearch.join(" ").toLowerCase().includes(filter.toLowerCase());
};

const hasSelectedTags = (tags: Tag[], selectedTags: string[]) => {
  if (selectedTags.length === 0) {
    return true;
  }
  return selectedTags.every((tag) => tags.map((t) => t.name).includes(tag));
};

const getAvailableTags = (chapters, resources) => {
  return [...(chapters || []), ...(resources || [])]
    .map((item: any) => item.tags || item.tags)
    .flat()
    .filter(
      (value: Tag, index: number, self: Tag[]) =>
        self.map((t: Tag) => t.name).indexOf(value.name) === index
    )
    .sort((a: Tag, b: Tag) => {
      if (a.type === b.type) {
        return a.name > b.name ? 1 : -1;
      }
      return a.type < b.type ? 1 : -1;
    });
};

type FormattedContentData = {
  filteredList: Chapter[] | Resource[];
  filteredContent: ReactElement;
  originalLength: number;
};
const formatContent = (
  chapters: Chapter[],
  resources: Resource[],
  selectedTags: string[],
  filter: string,
  pageStart: number,
  PAGE_LENGTH: number
): FormattedContentData => {
  if (chapters) {
    const filteredList = [...chapters].filter(
      (item) =>
        includeChapter(item, filter) && hasSelectedTags(item.tags, selectedTags) && !item.hideChapter
    );
    return {
      filteredList,
      filteredContent: (
        <>
          {filteredList.length > 0 && (
            <ChapterListContainer chapters={filteredList.slice(pageStart, pageStart + PAGE_LENGTH)} />
          )}
        </>
      ),
      originalLength: chapters.length,
    };
  }
  if (resources) {
    const filteredList = [...resources].filter(
      (item) =>
        includeResource(item, filter) &&
        hasSelectedTags(item.tags, selectedTags)
    );
    return {
      filteredList,
      filteredContent: (
        <>
          {filteredList.length > 0 && (
            <ListContainer>
              {filteredList
                .slice(pageStart, pageStart + PAGE_LENGTH)
                .map((resource: Resource) => ({
                  key: resource._id,
                  content: <ResourceListing resource={resource} />,
                }))}
            </ListContainer>
          )}
        </>
      ),
      originalLength: resources.length,
    };
  }
};

type PaginationProps = {
  pageStart: number;
  setPageStart: (value: number) => void;
  PAGE_LENGTH: number;
  filteredList: Chapter[] | Resource[];
};
function Pagination(props: PaginationProps) {
  const { pageStart, setPageStart, PAGE_LENGTH, filteredList } = props;
  const currentPage = Math.ceil(pageStart / PAGE_LENGTH) + 1;
  const numPages = Math.ceil(filteredList.length / PAGE_LENGTH);

  return (
    <NarrowContainer>
      <nav className="flex items-center justify-between border-t border-gray-200 px-4 sm:px-0">
        <div className="-mt-px flex w-0 flex-1">
          {currentPage !== 1 && (
            <button
              className="inline-flex items-center border-t-2 border-transparent pt-4 pr-1 text-sm text-slate-500 hover:border-slate-300 hover:text-slate-700"
              onClick={() => {
                const newStart = pageStart - PAGE_LENGTH;
                setPageStart(newStart >= 0 ? newStart : 0);
              }}
            >
              <AiOutlineArrowLeft
                className="mr-3 h-5 w-5 text-slate-400"
                aria-hidden="true"
              />
              Previous
            </button>
          )}
        </div>
        <div className="hidden md:-mt-px md:flex">
          <span className="inline-flex items-center border-t-2 border-transparent px-4 pt-4 text-sm text-slate-500">
            Page {currentPage} of {numPages}
          </span>
        </div>
        <div className="-mt-px flex w-0 flex-1 justify-end">
          {currentPage !== numPages && (
            <button
              className="inline-flex items-center border-t-2 border-transparent pt-4 pl-1 text-sm text-slate-500 hover:border-slate-300 hover:text-slate-700"
              onClick={() => {
                const newStart = pageStart + PAGE_LENGTH;
                setPageStart(newStart);
              }}
            >
              Next
              <AiOutlineArrowRight
                className="ml-3 h-5 w-5 text-slate-400"
                aria-hidden="true"
              />
            </button>
          )}
        </div>
      </nav>
    </NarrowContainer>
  );
}

type TagProps = {
  tag: Tag;
  selectedTags: string[];
  setSelectedTags: (value: string[]) => void;
  setPageStart: (value: number) => void;
};
function Tag(props: TagProps) {
  const { tag, selectedTags, setSelectedTags, setPageStart } = props;

  const tagClickHandler = (tagName: string) => {
    setPageStart(0);
    if (selectedTags.includes(tagName)) {
      setSelectedTags(selectedTags.filter((t) => t !== tagName));
      return;
    }
    setSelectedTags([...selectedTags, tagName]);
  };

  if (selectedTags.includes(tag.name)) {
    const colors =
      tag.type === "Subject"
        ? "bg-blue-600 border-blue-900"
        : "bg-green-600 border-green-900";

    return (
      <button
        type="button"
        className={`text-xs text-white border px-2 py-1 m-1 rounded-sm transition duration-300 ${colors}`}
        onClick={() => tagClickHandler(tag.name)}
      >
        {tag.name}
      </button>
    );
  }
  const colors =
    tag.type === "Subject"
      ? "text-blue-900 bg-blue-50 hover:bg-blue-600 border-blue-600 hover:border-blue-900"
      : "text-green-900 bg-green-50 hover:bg-green-600 border-green-600 hover:border-green-900";
  return (
    <button
      type="button"
      className={`text-xs hover:text-white border px-2 py-1 m-1 rounded-sm transition duration-300 ${colors}`}
      onClick={() => tagClickHandler(tag.name)}
    >
      {tag.name}
    </button>
  );
}

type FilterProps = {
  chapters?: Chapter[];
  resources?: Resource[];
};
export default function Filter(props: FilterProps) {
  const [filter, setFilter] = useState<string>("");
  const [selectedTags, setSelectedTags] = useState<string[]>([]);
  const [pageStart, setPageStart] = useState<number>(0);
  const { chapters, resources } = props;
  const PAGE_LENGTH = 8;
  const availableTags = getAvailableTags(chapters, resources);
  const { filteredList, filteredContent, originalLength } = formatContent(
    chapters,
    resources,
    selectedTags,
    filter,
    pageStart,
    PAGE_LENGTH
  );

  return (
    <>
      <NarrowContainer>
        <div className="flex">
          <label htmlFor="search" className="sr-only">
            Search
          </label>
          <input
            type="text"
            name="search"
            id="search"
            className="bg-white border rounded-md py-2 px-4 text-base text-gray-900 placeholder-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white focus:border-white focus:placeholder-gray-400 flex-auto"
            placeholder="Search and filter"
            onChange={(e) => {
              setFilter(e.target.value);
              setPageStart(0);
            }}
          />
          <span className="ml-4 py-2 text-slate-400 sm:text-sm">
            {filteredList.length} of {originalLength}
          </span>
        </div>
        <div className="flex flex-wrap px-2">
          {availableTags.map((tag: Tag) => (
            <Tag
              key={tag.name}
              tag={tag}
              selectedTags={selectedTags}
              setSelectedTags={setSelectedTags}
              setPageStart={setPageStart}
            />
          ))}
        </div>
      </NarrowContainer>
      {filteredContent}
      {/* Pagination when the list is larger than PAGE_LENGTH */}
      {filteredList.length > PAGE_LENGTH && (
        <Pagination
          pageStart={pageStart}
          setPageStart={setPageStart}
          PAGE_LENGTH={PAGE_LENGTH}
          filteredList={filteredList}
        />
      )}
      {/* Show message when there are no results */}
      {filteredList.length === 0 && (
        <NarrowContainer>
          <p className="mx-2 pt-12 text-slate-600 text-sm">
            No results, try unselecting some tags or removing some search/filter
            terms
          </p>
        </NarrowContainer>
      )}
    </>
  );
}

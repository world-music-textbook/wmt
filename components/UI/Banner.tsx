import Link from "next/link";

type BannerProps = {
  text: string;
  link?: string;
};

export default function Banner(props: BannerProps) {
  const { text, link } = props;
  return link ? (
    <Link href={link}>
      <a className="block cursor-pointer bg-red-700 py-10 text-center text-2xl text-white">
        {text}
      </a>
    </Link>
  ) : (
    <div className="bg-red-700 py-10 text-center text-2xl text-white">
      {text}
    </div>
  );
}

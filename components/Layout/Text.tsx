import React from "react";

type TextProps = {
  children: any;
  center?: boolean;
};

export function H1(props: TextProps) {
  const { children, center } = props;
  return (
    <h1
      className={`text-4xl md:text-5xl text-green-800 font-bold ${
        center && "text-center"
      }`}
    >
      {children}
    </h1>
  );
}

export function H2(props: TextProps) {
  const { children, center } = props;
  return (
    <h2
      className={`text-2xl text-green-800 font-bold ${center && "text-center"}`}
    >
      {children}
    </h2>
  );
}

export function H3(props: TextProps) {
  const { children, center } = props;
  return (
    <h3
      className={`text-xl text-green-600 font-bold ${center && "text-center"}`}
    >
      {children}
    </h3>
  );
}

export function H4(props: TextProps) {
  const { children, center } = props;
  return <h4 className="text-lg text-slate-900 font-semibold">{children}</h4>;
}

type AProps = {
  href?: string;
  children: any;
};
export const A = React.forwardRef<
  HTMLAnchorElement,
  React.PropsWithChildren<AProps>
>((props, ref) => {
  const { href, children } = props;
  return (
    <a
      href={href}
      className="hover:underline cursor-pointer text-green-700"
      ref={ref}
    >
      {children}
    </a>
  );
});

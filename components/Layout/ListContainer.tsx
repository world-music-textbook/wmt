import NarrowContainer from "@components/Layout/NarrowContainer";

export default function Example(props: {
  children: { key: string; content: any }[];
}) {
  const { children } = props;
  return (
    <NarrowContainer>
      <ul role="list" className="divide-y divide-gray-200">
        {children.map((child) => (
          <li key={child.key} className="py-4">
            {child.content}
          </li>
        ))}
      </ul>
    </NarrowContainer>
  );
}

import { ErrorMessage, useFormik } from "formik";
import { useState } from "react";
import {
  IoMailSharp,
  IoLogoFacebook,
  IoLogoInstagram,
  IoLogoTwitter,
  IoLogoGitlab,
} from "react-icons/io5";
import Script from "next/script";

type FooterProps = {
  siteConfig: {
    title: string;
    copyrightYear: string;
    footerColumns: {
      _key: string;
      header: string;
      entries: { _key: string; name: string; link: string }[];
    }[];
    social: {
      email?: string;
      facebook?: string;
      instagram?: string;
      twitter?: string;
      gitlab?: string;
    };
  };
};

export default function Footer(props: FooterProps) {
  const [contactFormSent, setContactFormSent] = useState<boolean>(false);
  const formikControl = useFormik({
    initialValues: {
      name: "",
      email: "",
      comments: "",
    },
    onSubmit: async (values, { setSubmitting, resetForm }) => {
      try {
        const { name, email, comments } = values;
        const res = await fetch("/api/signup", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ email, name, comments }),
        });
        resetForm();
        setContactFormSent(true);
        window.setTimeout(() => { setContactFormSent(false) }, 5000) 
      } catch (e) {
        console.log(e);
      }
      setSubmitting(false);
    },
  });

  return (
    <footer className="bg-slate-800" aria-labelledby="footer-heading">
      <h2 id="footer-heading" className="sr-only">
        Footer
      </h2>
      <div className="max-w-7xl mx-auto pt-12 space-y-12 px-4 sm:px-6 lg:pt-16 lg:px-8">
        <div className="md:grid md:grid-cols-2 md:gap-8 space-y-12 md:space-y-0">
          <div className="space-y-12">
            {props.siteConfig.footerColumns.map((column) => (
              <div key={column._key}>
                <h3 className="text-sm font-semibold text-slate-400 tracking-wider uppercase">
                  {column.header}
                </h3>
                <ul role="list" className="mt-4 space-y-4">
                  {column.entries.map((entry) => (
                    <li key={entry._key}>
                      <a
                        href={entry.link}
                        target="_blank"
                        className="text-base text-slate-100 hover:text-white"
                      >
                        {entry.name}
                      </a>
                    </li>
                  ))}
                </ul>
              </div>
            ))}
          </div>
          <div>
            <h3 className="text-sm font-semibold text-slate-400 tracking-wider uppercase">
              Leave feedback and subscribe
            </h3>
            <form className="space-y-4" onSubmit={formikControl.handleSubmit}>
              <p className="mt-4 text-base text-slate-100">
                Share your thoughts and add your email to sign up for our
                newsletter. We don't share info and will not fill your inbox.
              </p>
              <div>
                <label htmlFor="comments" className="sr-only">
                  Share your thoughts (optional)
                </label>
                <textarea
                  id="comments"
                  name="comments"
                  placeholder="Share your thoughts (optional)"
                  className="appearance-none min-w-0 w-full bg-white border border-transparent rounded-md py-2 px-4 text-base text-slate-900 placeholder-slate-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-slate-800 focus:ring-white focus:border-white focus:placeholder-slate-400"
                  onChange={formikControl.handleChange}
                  value={formikControl.values.comments}
                />
              </div>
              <div>
                <label htmlFor="name" className="sr-only">
                  Enter your name (optional)
                </label>
                <input
                  id="name"
                  name="name"
                  type="text"
                  placeholder="Enter your name (optional)"
                  className="appearance-none min-w-0 w-full bg-white border border-transparent rounded-md py-2 px-4 text-base text-slate-900 placeholder-slate-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-slate-800 focus:ring-white focus:border-white focus:placeholder-slate-400"
                  onChange={formikControl.handleChange}
                  value={formikControl.values.name}
                />
              </div>
              <div>
                <label htmlFor="email" className="sr-only">
                  Enter your email (optional)
                </label>
                <input
                  id="email"
                  name="email"
                  type="email"
                  placeholder="Enter your email (optional)"
                  className="appearance-none min-w-0 w-full bg-white border border-transparent rounded-md py-2 px-4 text-base text-slate-900 placeholder-slate-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-slate-800 focus:ring-white focus:border-white focus:placeholder-slate-400"
                  onChange={formikControl.handleChange}
                  value={formikControl.values.email}
                />
              </div>
              {formikControl.errors.email && (
                <p className="text-red-500">
                  <ErrorMessage name="email" />
                </p>
              )}
              <button
                type="submit"
                disabled={formikControl.isSubmitting}
                className="mt-4 w-full bg-blue-600 border border-transparent rounded-md py-2 px-4 items-center justify-center text-base font-medium text-white disabled:bg-blue-100 disabled:text-blue-400 hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-slate-800 focus:ring-blue-500"
              >
                {contactFormSent ? "Sent, thanks!" : "Submit" }
              </button>
            </form>
          </div>
        </div>
      </div>

      <div className="max-w-7xl mx-auto pb-12 px-4 sm:px-6 lg:pb-16 lg:px-8">
        <div className="mt-12 border-t border-slate-700 pt-8 lg:flex lg:items-center lg:justify-between">
          <div className="flex space-x-6 lg:order-2 lg:ml-20">
            {props.siteConfig.social.email && (
              <a
                href={`mailto:${props.siteConfig.social.email}`}
                className="text-slate-200 hover:text-slate-100"
              >
                <span className="sr-only">Email</span>
                <IoMailSharp className="h-6 w-6" aria-hidden="true" />
              </a>
            )}
            {props.siteConfig.social.facebook && (
              <a
                href={props.siteConfig.social.facebook}
                target="_blank"
                className="text-slate-200 hover:text-slate-100"
              >
                <span className="sr-only">Facebook</span>
                <IoLogoFacebook className="h-6 w-6" aria-hidden="true" />
              </a>
            )}
            {props.siteConfig.social.instagram && (
              <a
                href={props.siteConfig.social.instagram}
                target="_blank"
                className="text-slate-200 hover:text-slate-100"
              >
                <span className="sr-only">Instagram</span>
                <IoLogoInstagram className="h-6 w-6" aria-hidden="true" />
              </a>
            )}
            {props.siteConfig.social.twitter && (
              <a
                href={props.siteConfig.social.twitter}
                target="_blank"
                className="text-slate-200 hover:text-slate-100"
              >
                <span className="sr-only">Twitter</span>
                <IoLogoTwitter className="h-6 w-6" aria-hidden="true" />
              </a>
            )}
            {props.siteConfig.social.gitlab && (
              <a
                href={props.siteConfig.social.gitlab}
                target="_blank"
                className="text-slate-200 hover:text-slate-100"
              >
                <span className="sr-only">GitLab</span>
                <IoLogoGitlab className="h-6 w-6" aria-hidden="true" />
              </a>
            )}
          </div>
          <p className="space-y-4 mt-8 text-base text-slate-200 lg:mt-0 lg:order-1">
            <span className="block">
              &copy; {props.siteConfig.copyrightYear} {props.siteConfig.title}
            </span>
            <span className="block">
              Worldmusictextbook.org is open source and is maintained at{" "}
              <a
                href="https://gitlab.com/world-music-textbook"
                className="text-base text-slate-100 hover:text-white"
                target="_blank"
              >
                https://gitlab.com/world-music-textbook
              </a>
            </span>
            <span className="block">
              This work is licensed under a{" "}
              <a
                href="http://creativecommons.org/licenses/by-nc-nd/4.0/"
                className="text-base text-slate-100 hover:text-white"
                target="_blank"
              >
                Creative Commons Attribution — Non-Commercial — No Derivatives
                4.0 International License.
              </a>
            </span>
          </p>
        </div>
      </div>
      <Script
        data-goatcounter="https://worldmusic.goatcounter.com/count"
        strategy="afterInteractive"
        src="//gc.zgo.at/count.js"
      />
    </footer>
  );
}

import ChapterListing from "@components/UI/ChapterListing";

type ChapterListContainerProps = {
  chapters: Chapter[];
};

export default function ChapterListContainer(props: ChapterListContainerProps) {
  return (
    <div className="pt-12 lg:grid lg:grid-cols-2">
      {props.chapters.map((chapter, index) => (
        <ChapterListing chapter={chapter} key={chapter._id} index={index} />
      ))}
    </div>
  );
}

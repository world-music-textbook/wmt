type Author = {
  _createdAt: string;
  _id: string;
  _rev: string;
  _type: "author";
  _updatedAt: string;
  affiliation: string;
  bio: any[];
  image: Image;
  name: string;
  role: string;
};

type Chapter = {
  _createdAt: string;
  _id: string;
  _rev: string;
  _type: "chapter";
  _updatedAt: string;
  author: Author[];
  hideChapter: boolean;
  body: any[];
  description: string;
  doiLink: string;
  image: Image;
  multiPage: boolean;
  multiPageBody: { sectionTitle: string; sectionBody: any[] };
  publishedAt: string;
  slug: Slug;
  tags: Tag[];
  title: string;
  codePath: string;
};

type Image = {
  _type: "image";
  alt: string;
  asset: any;
  crop: {
    _type: "sanity.imageCrop";
    bottom: number;
    left: number;
    right: number;
    top: number;
  };
  hotspot: {
    _type: "sanity.imageCrop";
    height: number;
    width: number;
    x: number;
    y: number;
  };
};

type Post = {
  _createdAt: string;
  _id: string;
  _rev: string;
  _type: "post";
  _updatedAt: string;
  author: Author[];
  body: any[];
  description: string;
  image: Image;
  publishedAt: string;
  slug: Slug;
  title: string;
};

type Resource = {
  _createdAt: string;
  _id: string;
  _rev: string;
  _type: "resource";
  _updatedAt: string;
  author?: string;
  date: number;
  description: any[];
  link?: string;
  publisher?: string;
  tags: Tag[];
  title: string;
  type: string;
};

type Slug = {
  _type: "slug";
  current: string;
};

type Tag = {
  _createdAt: string;
  _id: string;
  _rev: string;
  _type: "tag";
  _updatedAt: string;
  name: string;
  type: "Subject" | "Geography";
};

type Section = {
  _key: string;
  sectionBody: any[];
  sectionTitle: string;
};

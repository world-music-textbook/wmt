const STUDIO_REWRITE = {
  source: "/admin/:path*",
  destination:
    process.env.NODE_ENV === "development"
      ? "http://localhost:3333/admin/:path*"
      : "/admin/index.html",
};

module.exports = {
  images: {
    domains: ["cdn.sanity.io"],
  },
  async redirects() {
    return [
      {
        source: "/alexander-2024b",
        destination: "/chapters/alexander-2024b",
        permanent: true,
      },
      {
        source: "/alexander-2024a",
        destination: "/chapters/alexander-2024a",
        permanent: true,
      },
      {
        source: "/summers-2023",
        destination: "/chapters/summers-2023",
        permanent: true,
      },
      {
        source: "/llano-2023",
        destination: "/chapters/llano-2023",
        permanent: true,
      },
      {
        source: "/sur-2023",
        destination: "/chapters/sur-2023",
        permanent: true,
      },
      {
        source: "/koons-2023",
        destination: "/chapters/koons-2023",
        permanent: true,
      },
      {
        source: "/nielsen-vallejo-2022",
        destination: "/chapters/nielsen-vallejo-2022",
        permanent: true,
      },
      {
        source: "/nielsen-vallejo-playlist-2022",
        destination: "/chapters/nielsen-vallejo-playlist-2022",
        permanent: true,
      },
      {
        source: "/mcnally-2021",
        destination: "/chapters/mcnally-2021",
        permanent: true,
      },
      {
        source: "/song-grant-2021",
        destination: "/chapters/song-grant-2021",
        permanent: true,
      },
      {
        source: "/witulski-2021",
        destination: "/chapters/witulski-2021",
        permanent: true,
      },
      {
        source: "/nielsen-2020",
        destination: "/chapters/nielsen-2020",
        permanent: true,
      },
    ];
  },
  i18n: {
    locales: ["en"],
    defaultLocale: "en",
  },
  rewrites: () => [STUDIO_REWRITE],
  webpack(config, options) {
    const { isServer } = options;
    config.module.rules.push({
      test: /\.(ogg|mp3|wav|aiff|mpe?g)$/i,
      exclude: config.exclude,
      use: [
        {
          loader: require.resolve("url-loader"),
          options: {
            limit: config.inlineImageLimit,
            fallback: require.resolve("file-loader"),
            publicPath: `${config.assetPrefix}/_next/static/images/`,
            outputPath: `${isServer ? "../" : ""}static/images/`,
            name: "[name]-[hash].[ext]",
            esModule: config.esModule || false,
          },
        },
      ],
    });
    return config;
  },
};

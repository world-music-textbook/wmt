# World Music Textbook

This repository houses the code for the _World Music Textbook_ Website. The site uses [NextJS](https://nextjs.org/) and [Sanity](https://www.sanity.io/), providing what we hope can be a useful template for future similar projects. Please fork away and dig in or submit any bugs or issues.

If you would like to contact the editors directly about questions or comments related to comments, the easiest way will be through the contact form in the website footer or the email listed on the site.

## Getting started

1. Fork or clone the repository.
2. Run `npm install` to download and install of the required libraries.
3. Create a `.env` file (or a `.env.local` or similar for different environments), copy the contents of `.env.sample` and add your own contents. _Note: as of this writing, we have not included checks against empty fields, so if you are not using some of the features we have added to our project, you may need to comment out relevant code to get things running._
4. Open two terminal windows and navigate to the project directory. In one, run `npm run dev` and in the other run `npm run sanity`.
5. In one browser window, open `localhost:3000` to load the site. In another, open `localhost:3000/admin` to open the Sanity interface, which is the content manager where you can edit much of the site text.

This site takes advantage of many features of NextJS and Sanity, but there has not been much interest driving efforts to document our implementation. If you are interested in starting a project, reach out to the editors and we can discuss how to best make things work for you.

import { HiOutlineDocumentText } from "react-icons/hi";

export default {
  name: "page",
  title: "Page",
  icon: HiOutlineDocumentText,
  type: "document",
  fields: [
    {
      name: "title",
      title: "Title",
      type: "string",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "titlePart2",
      title: "Title part 2",
      description:
        "An optional part of the title to show in a contrasting color",
      type: "string",
    },
    {
      name: "slug",
      title: "Slug",
      type: "slug",
      validation: (Rule) => Rule.required(),
      options: {
        source: "title",
        maxLength: 96,
      },
    },
    {
      name: "description",
      description:
        "Write a short pararaph of this post (For display in the hero image and SEO Purposes)",
      title: "Description",
      rows: 5,
      type: "text",
      validation: (Rule) =>
        Rule.max(160).error(
          "SEO descriptions are usually better when its below 160"
        ),
    },
    {
      name: "cta",
      type: "object",
      description:
        "Text and links for the hero buttons (optional: none, 1, both)",
      title: "Hero buttons",
      fields: [
        {
          title: "Button 1",
          type: "object",
          name: "cta1",
          fields: [
            {
              title: "Button text",
              type: "string",
              name: "text",
              validation: (Rule) => Rule.required(),
            },
            {
              title: "Link",
              type: "string",
              name: "link",
              validation: (Rule) => Rule.required(),
            },
          ],
        },
        {
          title: "Button 2",
          type: "object",
          name: "cta2",
          fields: [
            {
              title: "Button text",
              type: "string",
              name: "text",
              validation: (Rule) => Rule.required(),
            },
            {
              title: "Link",
              type: "string",
              name: "link",
              validation: (Rule) => Rule.required(),
            },
          ],
        },
      ],
    },
    {
      name: "body",
      title: "Body",
      type: "portableText",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "image",
      title: "Hero image",
      type: "image",
      validation: (Rule) => Rule.required(),
      fields: [
        {
          name: "alt",
          type: "string",
          title: "Alternative text",
          description: "Important for SEO and accessiblity.",
          validation: (Rule) => Rule.required(),
          options: {
            isHighlighted: true,
          },
        },
      ],
      options: {
        hotspot: true,
      },
    },
  ],

  preview: {
    select: {
      title: "title",
      titlePart2: "titlePart2",
      media: "mainImage",
      author: "author",
    },
    prepare(selection) {
      const { title, titlePart2, author } = selection;
      return Object.assign({}, selection, {
        title: [title, titlePart2].join(" "),
        subtitle: author && `by ${author}`,
      });
    },
  },
};

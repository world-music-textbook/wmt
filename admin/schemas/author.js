import { HiOutlineUserAdd } from "react-icons/hi";

export default {
  name: "author",
  title: "Author",
  type: "document",
  icon: HiOutlineUserAdd,
  fields: [
    {
      name: "name",
      title: "Name",
      type: "string",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "role",
      title: "Role",
      type: "string",
    },
    {
      name: "affiliation",
      title: "Affiliation",
      type: "string",
    },
    {
      name: "bio",
      title: "Biography",
      type: "portableText",
    },
    {
      name: "image",
      title: "Image",
      type: "image",
      fields: [
        {
          name: "alt",
          type: "string",
          title: "Alternative text",
          description: "Important for SEO and accessibility.",
          options: {
            isHighlighted: true,
          },
          validation: (Rule) => Rule.required(),
        },
      ],
      options: {
        hotspot: true,
      },
    },
  ],

  preview: {
    select: {
      title: "name",
      subtitle: "affiliation",
      media: "image",
    },
  },
};
